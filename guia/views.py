from django.views.generic import TemplateView
from django.shortcuts import render, redirect
from guia.models import Alcance, Planeacion
from guia.forms import AlcanceForm, RegistrationForm, PlaneacionForm
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic
import mimetypes
from django.contrib import messages

# Create your views here.
# def homePageView(request):
# return HttpResponse('Hello World!')


class HomePageView(TemplateView):
    template_name = 'home.html'


class AboutPageView(TemplateView):
    template_name = 'acerca.html'

class TestPageView(TemplateView):
    template_name = 'guia.html'

    def post(self, request):
        form = PlaneacionForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.user = request.user
            post.save()
            messages.success(request, "Guardado correctamente")

            form = PlaneacionForm()
            return redirect('ruta')

        args = {'form': form}
        return render(request, self.template_name, args)



class GuiaPageView(TemplateView):
    template_name = 'guia.html'

    def get(self, request):
        form = AlcanceForm()
        form2 = PlaneacionForm()
        data = Alcance.objects.filter(user=request.user)
        datos = Planeacion.objects.filter(user=request.user)

        args = {'form': form, 'data' : data, 'form2': form2, 'datos': datos}
        return render(request, self.template_name, args)


    def post(self, request):
        form = AlcanceForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.user = request.user
            post.save()
            messages.success(request, "Guardado correctamente")

            form = AlcanceForm()
            return redirect('ruta')

        args = {'form': form}
        return render(request, self.template_name, args)




class SignUpView(generic.CreateView):
    form_class = RegistrationForm
    success_url = reverse_lazy('login')
    template_name = 'registration/signup.html'

class CardPageView(TemplateView):
    template_name = 'cartas.html'

class AboutCardPageView(TemplateView):
    template_name = 'acerca_cartas.html'

class HowCardPageView(TemplateView):
    template_name = 'como_funcionan.html'

class WhenCardPageView(TemplateView):
    template_name = 'cuando_usarlas.html'

class AllCardPageView(TemplateView):
    template_name = 'all_cards.html'

class BestPracticesView(TemplateView):
    template_name = 'buenas_practicas.html'

class TipsView(TemplateView):
    template_name = 'tips.html'

class JuegoView(TemplateView):
    template_name = 'juego.html'

class JuegoSerioView(TemplateView):
    template_name = 'juego_serio.html'

class LSView(TemplateView):
    template_name = 'lean_startup.html'

class PLView(TemplateView):
    template_name = 'playing_lean.html'

class FJView(TemplateView):
    template_name = 'flujo_juego.html'


def eliminar_goal(request, id):
    goal = Alcance.objects.get(id=id)
    goal.delete()
    messages.success(request, "Eliminación correcta")
    return redirect('ruta')

def eliminar_plan(request, id):
    plan = Planeacion.objects.get(id=id)
    plan.delete()
    messages.success(request, "Eliminación correcta")

    return redirect('ruta')


def editar_alcance(request, id):
    form = AlcanceForm()
    alcance = Alcance.objects.filter(id=id)

    args = {'form': form, 'alcance' : alcance}
    return render(request, "update.html", args)


def actualizar_alcance(request, id):
    alcance = Alcance.objects.get(pk=id)
    form = AlcanceForm(request.POST, instance=alcance)
    if form.is_valid():
        post = form.save(commit=False)
        post.user = request.user
        post.save()
        messages.success(request, "Actualización correcta")

        form = AlcanceForm()
        return redirect('ruta')

    args = {'form': form}
    return render(request, args)

def editar_planeacion(request, id):
    form = PlaneacionForm()
    plan = Planeacion.objects.filter(id=id)

    args = {'form': form, 'plan' : plan}
    return render(request, "edit_plan.html", args)

def actualizar_planeacion(request, id):
    plan = Planeacion.objects.get(pk=id)
    form = PlaneacionForm(request.POST, instance=plan)
    if form.is_valid():
        update = form.save(commit=False)
        update.user = request.user
        update.save()
        messages.success(request, "Actualización correcta")

        form = PlaneacionForm()
        return redirect('ruta')
