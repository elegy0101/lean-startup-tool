from django.contrib import admin

from guia.models import Alcance, Planeacion
admin.site.register(Alcance)
admin.site.register(Planeacion)
