from django.forms import ModelForm
from guia.models import Alcance, Planeacion
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class AlcanceForm(ModelForm):
    class Meta:
        model = Alcance
        fields = ('goal','contexto','juego',)


class RegistrationForm(UserCreationForm):
    password1 = forms.CharField(max_length=30)
    password2 = None

    class Meta:
        model = User
        fields = ('username', 'password1', )


class PlaneacionForm(ModelForm):
    class Meta:
        model = Planeacion
        fields = ('modalidad','fecha','hora','lugar','materiales','jugadores',)
