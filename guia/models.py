from django.db import models
from django.contrib.auth.models import User



class Alcance(models.Model):
    user = models.ForeignKey(User, default=None, on_delete=models.CASCADE)
    goal = models.CharField(max_length=500)
    contexto = models.CharField(max_length=500)
    juego = models.CharField(max_length=500)


class Planeacion(models.Model):
    user = models.ForeignKey(User, default=None, on_delete=models.CASCADE)
    modalidad = models.CharField(max_length=500)
    fecha = models.CharField(max_length=500)
    hora = models.CharField(max_length=500)
    lugar = models.CharField(max_length=500)
    materiales = models.CharField(max_length=500)
    jugadores = models.CharField(max_length=500)
