# guia/urls.py
from django.urls import path
from .views import HomePageView, AboutPageView, GuiaPageView, SignUpView, TestPageView, CardPageView, AboutCardPageView, HowCardPageView, WhenCardPageView, AllCardPageView, BestPracticesView, TipsView, JuegoView, JuegoSerioView, LSView, PLView, FJView
from . import views


urlpatterns = [
    path('ruta/', GuiaPageView.as_view(), name="ruta"), #url de la guia
    path('acerca/', AboutPageView.as_view(), name="acerca"), #url de acerca de
    path('', HomePageView.as_view(), name="home"), #url de inicio
    path('accounts/registrar/', SignUpView.as_view(), name='registrar'),
    path('test/', TestPageView.as_view(), name="test"), #url para enviar form
    path('cartas-de-experimentos/', CardPageView.as_view(), name="cartas-de-experimentos"), #url de cartas
    path('acerca-de-las-cartas/', AboutCardPageView.as_view(), name="acerca-de-las-cartas"), #url acerca de las cartas
    path('como-funcionan-las-cartas/', HowCardPageView.as_view(), name="como-funcionan-las-cartas"), #url sobre como funcionan las cartas
    path('cuando-usar-las-cartas/', WhenCardPageView.as_view(), name="cuando-usar-las-cartas"), #url sobre cuando usar las cartas
    path('todas-las-cartas/', AllCardPageView.as_view(), name="todas-las-cartas"), #url que muestra todas las cartas
    path('buenas-practicas/', BestPracticesView.as_view(), name="buenas-practicas"), #url que muestra las buenas practicas
    path('juego-serio/', JuegoView.as_view(), name="juego-serio"), #url que muestra las recomendaciones
    path('recomendaciones/', TipsView.as_view(), name="recomendaciones"), #url que muestra las recomendaciones
    path('que-es-un-juego-serio/', JuegoSerioView.as_view(), name="que-es-un-juego-serio"), #url que muestra las recomendaciones
    path('lean-startup/', LSView.as_view(), name="lean-startup"), #url que muestra las recomendaciones
    path('playing-lean/', PLView.as_view(), name="playing-lean"), #url que muestra las recomendaciones
    path('flujo-del-juego/', FJView.as_view(), name="flujo-del-juego"), #url que muestra las recomendaciones
    path('eliminar-alcance/<int:id>', views.eliminar_goal, name="eliminar-alcance"), #url que muestra las recomendaciones
    path('eliminar-planeacion/<int:id>', views.eliminar_plan, name="eliminar-planeacion"), #url que muestra las recomendaciones
    path('editar-alcance/<int:id>', views.editar_alcance, name="editar-alcance"), #url que muestra las recomendaciones
    path('actualizar-alcance/<int:id>', views.actualizar_alcance, name="actualizar-alcance"), #url que muestra las recomendaciones
    path('editar-planeacion/<int:id>', views.editar_planeacion, name="editar-planeacion"), #url que muestra las recomendaciones
    path('actualizar_planeacion/<int:id>', views.actualizar_planeacion, name="actualizar_planeacion"), #url que muestra las recomendaciones

]
